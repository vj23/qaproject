package HomeWork;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethodClass;

public class CreateLead extends ProjectMethodClass{
//	@Parameters({"companyname","firstname","lastname"})
	
//	@Test
	//@Test(groups="Smoke")
	//@Test(invocationCount=2 ,timeOut=30000)
	
	public void createleadwithparameter(String cn,String fn, String ln)
	{
   
	WebElement ClckCrmSfa = locateElement("partialtext", "CRM/SFA");
	click(ClckCrmSfa);
	WebElement ClckLeads = locateElement("partialtext", "Leads");
	click(ClckLeads);
	WebElement ClckCreateLeads = locateElement("partialtext", "Create Lead");
	click(ClckCreateLeads);
	WebElement EntCusName = locateElement("id", "createLeadForm_companyName");
	clearAndType(EntCusName, "Siemens");
	WebElement EntFirstName = locateElement("id", "createLeadForm_firstName");
	clearAndType(EntFirstName, "Vijay");
	WebElement EntLastName = locateElement("id", "createLeadForm_lastName");
	clearAndType(EntLastName, "kumar");
	WebElement SubmmitLeads = locateElement("name", "submitButton");
    SubmmitLeads.click();
}
}

