package HomeWork;

import org.testng.annotations.Test;


public class ClassPriority {

	@Test(priority=3)
	public void A()
	{
		System.out.println("One");
	}
	@Test(priority=-1)
	public void B()
	{
		System.out.println("two");
		
	}
	@Test(priority=2)
//	@Test(dependsOnMethods="two")
	public void C()
	{
		System.out.println("three");
	}
}

