package bi.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;

import bl.framework.api.SeleniumBase;
import bl.framework.testcases.FetchDataFM;


public class ProjectMethodClass extends SeleniumBase {
//	@Parameters({"url","username","password"})
    @BeforeMethod
//	@BeforeMethod(groups="Common")
//	public void login(String url,String un,String pwd) 
    public void login(){

	startApp("chrome", "http://leaftaps.com/opentaps/control/main");
	WebElement eleUsername = locateElement("id", "username");
	clearAndType(eleUsername, "DemoSalesManager"); 
	WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, "crmsfa"); 
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin); 
	
	
}
    @DataProvider(name="fetchData")
    public Object[][] getdata()
    {
    	return FetchDataFM.readData(datasheetname);
    }
}
