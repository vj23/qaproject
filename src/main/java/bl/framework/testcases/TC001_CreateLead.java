package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethodClass;

public class TC001_CreateLead extends ProjectMethodClass{
	

	
	@Test
//	@Test(dataProvider="getdatas")
//	@Test(groups="Smoke")
	//@Test(invocationCount=2 ,timeOut=30000)
	public void createlead(String cn,String fn,String ln)
	{
   
	WebElement ClckCrmSfa = locateElement("partialtext", "CRM/SFA");
	click(ClckCrmSfa);
	WebElement ClckLeads = locateElement("partialtext", "Leads");
	click(ClckLeads);
	WebElement ClckCreateLeads = locateElement("partialtext", "Create Lead");
	click(ClckCreateLeads);
	WebElement EntCusName = locateElement("id", "createLeadForm_companyName");
	clearAndType(EntCusName, cn);
	WebElement EntFirstName = locateElement("id", "createLeadForm_firstName");
	clearAndType(EntFirstName, fn);
	WebElement EntLastName = locateElement("id", "createLeadForm_lastName");
	clearAndType(EntLastName, ln);
	WebElement SubmmitLeads = locateElement("name", "submitButton");
    SubmmitLeads.click();
}
	
//	@DataProvider(name="getdatas")
//	public String[][] InputData() {
//		String[][] data=new String[2][3];
//		data[0][0]="TestLeaf";
//		data[0][1]="Sid";
//		data[0][2]="cjvk";
//		
//		data[1][0]="Siemenes";
//		data[1][1]="Vijay";
//		data[1][2]="jaison";
//		return data;
	}
}
