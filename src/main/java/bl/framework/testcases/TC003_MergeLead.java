package bl.framework.testcases;


	import org.openqa.selenium.WebElement;
	import org.testng.annotations.Test;

	import bi.framework.design.ProjectMethodClass;

	public class TC003_MergeLead extends ProjectMethodClass{
	@Test
	public void merge() throws InterruptedException
	{
		WebElement ClickCRMSFA = locateElement("partialtext","CRM/SFA");
		click(ClickCRMSFA);
		
		WebElement ClickLeads = locateElement("xpath","//a[text()='Leads']");
		click(ClickLeads);
		WebElement ClickMergeLeads = locateElement("xpath","//a[text()='Merge Leads']");
		click(ClickMergeLeads);
		WebElement ClickFromLeadIcon = locateElement("xpath"," (//input[@id='partyIdFrom']/following::a/img)[1]"); 
		click(ClickFromLeadIcon);
		switchToWindow(1);
	    WebElement EnterFromID = locateElement("name","id");
		clearAndType(EnterFromID, "10029");
		WebElement ClickFindLead = locateElement("xpath","//button[text()='Find Leads']");
		click(ClickFindLead);
		Thread.sleep(3000);
		WebElement ClickID = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(ClickID);
		Thread.sleep(3000);
		switchToWindow(0);
		WebElement ClickToLeadIcon = (locateElement("xpath","//table[@id='widget_ComboBox_partyIdTo']//following::a[1]")); 
		click(ClickToLeadIcon);
		switchToWindow(1);
	    WebElement EnterToID = locateElement("name","id");
		clearAndType(EnterToID, "10030");
		WebElement ClickToFindLead = locateElement("xpath","//button[text()='Find Leads']");
		click(ClickToFindLead);
		Thread.sleep(3000);
		WebElement ClickToID = locateElement("xpath","//a[text()='10030']");
		click(ClickToID);
		switchToWindow(0);
		WebElement ClickMergeLead = locateElement("xpath","//a[text()='Merge']");
		click(ClickMergeLead);
		switchToAlert();
		
		
	}
		
}
	