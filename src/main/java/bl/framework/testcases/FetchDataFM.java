package bl.framework.testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchDataFM {
	
	XSSFWorkbook wbook; 

	static Object[][] data;
	public static Object[][] readData(String datasheetname) 
	{
	
		try {
		XSSFWorkbook wbook=new XSSFWorkbook("./Datas/"+datasheetname);//Enters workbook
		XSSFSheet sheet=wbook.getSheet("Sheet1");//Enters sheet
		
		int rowcount=sheet.getLastRowNum();//get last row number
		System.out.println(rowcount);
		
		int colcount=sheet.getRow(0).getLastCellNum();//gets last cell number from sheet
		System.out.println(colcount);
		data=new Object[rowcount][colcount];
		
		for(int i=1; i<=rowcount; i++)
		{
			XSSFRow row=sheet.getRow(i);//moves to row number
			for(int j=0; j<colcount;j++)
			{
				XSSFCell cell=row.getCell(j);//moves to cell number
//				String text=cell.getStringCellValue();//get value from that cell
//				System.out.println(text);
				data[i-1][j]=cell.getStringCellValue();
				
			}
		}
		}
	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
		
		
		return data;
}
}

