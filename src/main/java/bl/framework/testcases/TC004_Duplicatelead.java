package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethodClass;

public class TC004_Duplicatelead extends ProjectMethodClass {
//	@Test
//    @Test(groups="Smoke")
	public void duplicate()
	{
	
	WebElement ClckCrmSfa = locateElement("partialtext", "CRM/SFA");
	click(ClckCrmSfa);
	WebElement ClckLeads = locateElement("partialtext", "Leads");
	click(ClckLeads);
	WebElement ClckCreateLeads = locateElement("partialtext", "Create Lead");
	click(ClckCreateLeads);
	WebElement EntCusName = locateElement("id", "createLeadForm_companyName");
	clearAndType(EntCusName, "Siemens");
	WebElement EntFirstName = locateElement("id", "createLeadForm_firstName");
	clearAndType(EntFirstName, "sid");
	WebElement EntLastName = locateElement("id", "createLeadForm_lastName");
	clearAndType(EntLastName, "Kumar");
	WebElement SubmmitLeads = locateElement("name", "submitButton");
    SubmmitLeads.click();
    WebElement ClickDuplicate = locateElement("partialtext", "Duplicate Lead");
    ClickDuplicate.click();
    WebElement DuplicateName = locateElement("id", "createLeadForm_firstName");
    clearAndType(DuplicateName, "CJVK");
    WebElement SubmmitLeads1 = locateElement("name", "submitButton");
    SubmmitLeads1.click();

}
}