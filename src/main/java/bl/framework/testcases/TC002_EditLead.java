package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethodClass;

public class TC002_EditLead extends ProjectMethodClass {

//@Test(groups="Sanity")
		
//@Test(dependsOnMethods="bl.framework.testcases.TC001_CreateLead.createlead")
 public void editLead() throws InterruptedException
 {
	
	WebElement ClckCrmSfa = locateElement("partialtext", "CRM/SFA");
	click(ClckCrmSfa);
	WebElement ClckLeads = locateElement("partialtext", "Leads");
	click(ClckLeads);
	WebElement EditLeads = locateElement("partialtext", "Find Leads");
	click(EditLeads);
	WebElement Leadid = locateElement("name","id");
	clearAndType(Leadid,"10008");
	WebElement ClickFindLeadButton = locateElement("xpath","//button[text()='Find Leads']");
	click(ClickFindLeadButton);
	Thread.sleep(2000);
	WebElement ClickId = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
	click(ClickId);
	WebElement ClickEditbutton = locateElement("partialtext", "Edit");
	click(ClickEditbutton);
	WebElement Updatename = locateElement("id", "updateLeadForm_companyName");
	clearAndType(Updatename, "Systhink");
	WebElement update = locateElement("name","submitButton");
	click(update);
	
	
	
}
}