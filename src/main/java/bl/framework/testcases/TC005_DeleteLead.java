package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethodClass;

public class TC005_DeleteLead extends ProjectMethodClass {

	@Test
	public void deletelead()
	{
		WebElement ClickCRMSFA = locateElement("partialtext","CRM/SFA");
		click(ClickCRMSFA);
		WebElement ClickLeads = locateElement("xpath","//a[text()='Leads']");
		click(ClickLeads);
		WebElement ClickFindLeads = locateElement("xpath","//a[text()='Find Leads']");
		click(ClickFindLeads);
	   WebElement EnterFirstname = locateElement("xpath","(//input[@name='firstName'])[3]");
       clearAndType(EnterFirstname, "Test1");
       WebElement ClickFindleadButton = locateElement("xpath","//button[text()='Find Leads']");
       click(ClickFindleadButton);

//		WebElement Leadid = locateElement("name","id");
//		clearAndType(Leadid,"10008");
WebElement ClickNameLink = locateElement("xpath","//a[text()='Test1']");
 click(ClickNameLink );
		WebElement Deletelead = locateElement("xpath","//a[text()='Delete']");
		click(Deletelead );
		
		
	}
}
